// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _UTILITY_H
#define _UTILITY_H

void usleep(int usecs);

#endif // _UTILITY_H