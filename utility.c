// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#include <generated/soc.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define TICKS_PER_US (CONFIG_CLOCK_FREQUENCY / 1000000LL)

#include "utility.h"

static __inline__ unsigned long long rdtsc(void)
{
    unsigned long long int tsc;
    unsigned long int high;
    unsigned long int low;
    unsigned long int scratch;
    __asm__ volatile("0:		\n"
                     "\tmftbu %0	\n"
                     "\tmftb  %1	\n"
                     "\tmftbu %2	\n"
                     "\tcmpw  %2,%0	\n"
                     "\tbne   0b	\n"
                     : "=r"(high), "=r"(low), "=r"(scratch));
    tsc = high << 32;
    tsc |= low;

    return tsc;
}

void usleep(int usecs)
{
    unsigned long long start_tsc;
    unsigned long long current_tsc;
    unsigned long long offset;

    offset = 0;
    start_tsc = rdtsc();

    while (1)
    {
        current_tsc = rdtsc();
        if (current_tsc < start_tsc)
        {
            offset = ULONG_MAX - start_tsc;
            start_tsc = 0;
        }
        if (((current_tsc - start_tsc) + offset) >= (TICKS_PER_US * usecs))
        {
            break;
        }
    }
}